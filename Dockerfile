# Dockerfile to build and run the module on the cloud
# Stage 1 python build
FROM python:3.9 AS builder
RUN apt-get update
RUN apt-get install wget
RUN apt-get install zip unzip -y

# FROM alpine:latest AS builder

RUN mkdir app
WORKDIR /app
ARG WGET_PULL
RUN wget -q "https://gitlab.com/api/v4/projects/37491970/repository/archive.zip?private_token=$WGET_PULL"
RUN unzip archive.zip*
RUN rm archive.zip*
RUN mv template*/* /app
ARG WGET_PULL
RUN wget -q "https://gitlab.com/api/v4/projects/37653617/repository/archive.zip?private_token=$WGET_PULL"
RUN unzip archive.zip*
RUN rm archive.zip*
RUN mv jigyasa-uniform*/* /app
COPY ./ /app
RUN pip install --no-cache-dir --upgrade -r requirements.txt
RUN python3 script.py

# Stage 2 httpd serving
FROM alpine:latest
RUN apk update \
    && apk add lighttpd \
    && rm -rf /var/cache/apk/*
WORKDIR /var/www/localhost/htdocs
COPY --from=builder /app /var/www/localhost/htdocs
EXPOSE 80
CMD ["lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]
